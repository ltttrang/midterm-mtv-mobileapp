package com.pickme.mtv.pickme;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;

import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.Task;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.mapping.MapFragment;
import com.pickme.mtv.pickme.heremap.HereMap;

public class MapActivity extends RuntimePermissionAcitivity implements UI {
    public static final int TIMEOUT = 10;
    public static final int INTERVAL = 5000;
    public static final int FAST_INTERVAL = 2500;
    public static final int SERVICE_SETTING_REQUEST_CODE = 1;
    public static final int SERVICE_AVAILABILITY_REQUEST_CODE = 2;

    private boolean mLocationReady = false;

    private RadioButton mReadyRadioBtn;
    private RadioButton mStandbyRadioBtn;
    private View mLocateBtn;

    private Button btnBegin;
    private Button btnEnd;

    private FrameLayout info;
    private TextView info_name;
    private TextView info_address;
    private TextView info_phone;
    private TextView info_note;

    @Nullable
    private RequestData mRequestData;
    private boolean mAccepted = false;

    private DriverClient mDriverClient;
    private String mAccessToken;
    private String mRefreshToken;

    private FusedLocationProviderClient mLocationProvider;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;

    private HereMap mHereMap;

    private static final String[] REQUEST_STATUP_PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_WIFI_STATE};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Bundle data = getIntent().getExtras();
        if (data == null) {
            finish();
            return;
        }

        DriverData driverData = data.getParcelable("driver_data");
        String url = data.getString("url");
        mAccessToken = data.getString("access_token");
        mRefreshToken = data.getString("refresh_token");

        mDriverClient = new DriverClient(this, driverData);

        try {
            mDriverClient.connect(url, mAccessToken, mRefreshToken);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        if (!checkServicesAvailibility()) {
            return;
        }

        mLocationProvider = LocationServices.getFusedLocationProviderClient(this);
        setupLocationServiceSetting();

        requestPermissions(REQUEST_STATUP_PERMISSIONS, new GrantResultListener() {
            @Override
            public void onGranted() throws SecurityException {
                initView();
                setupView();
            }

            @Override
            public void onDeny() {
                showPermissionError();
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, new GrantResultListener() {
            @Override
            public void onGranted() throws SecurityException {
                mLocationProvider.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
            }

            @Override
            public void onDeny() {
                showPermissionError();
            }
        });

        if (!mAccepted) {
            if (mReadyRadioBtn.isChecked()) {
                mDriverClient.updateStatus(1);
            } else {
                mDriverClient.updateStatus(0);
            }
        }
    }

    @Override
    protected void onPause() {
        if (!mAccepted) {
            mDriverClient.updateStatus(0);
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        mLocationProvider.removeLocationUpdates(mLocationCallback);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        mDriverClient.disconnect();
        finish();
        super.onDestroy();
    }

    @Override
    public void onConnected() {

    }

    private void setupView() {
        mReadyRadioBtn.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                mDriverClient.updateStatus(1);
            }
        });

        mStandbyRadioBtn.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                mDriverClient.updateStatus(0);
            }
        });

        mLocateBtn.setOnClickListener((v) -> {
            locate();
        });

        btnBegin.setOnClickListener(v -> {
            btnBegin.setEnabled(false);
            btnEnd.setEnabled(true);
            mHereMap.removeRoute();
            mHereMap.removeMarker("user");
            mDriverClient.begin(mRequestData);
        });

        btnEnd.setOnClickListener(v -> {
            btnBegin.setVisibility(View.INVISIBLE);
            btnEnd.setVisibility(View.INVISIBLE);
            mReadyRadioBtn.setEnabled(true);
            mStandbyRadioBtn.setEnabled(true);
            info.setVisibility(View.INVISIBLE);
            mAccepted = false;
            mDriverClient.end(mRequestData);
            mRequestData = null;
        });
    }

    private void initView() {
        mReadyRadioBtn = findViewById(R.id.ready_btn);
        mStandbyRadioBtn = findViewById(R.id.standby_btn);
        mLocateBtn = findViewById(R.id.locate_btn);
        btnBegin = findViewById(R.id.btnBegin);
        btnEnd = findViewById(R.id.btnEnd);
        info = findViewById(R.id.info);
        info_name = findViewById(R.id.txt_tenkhachhang);
        info_address = findViewById(R.id.txt_diachi);
        info_phone = findViewById(R.id.txt_sodienthoai);
        info_note = findViewById(R.id.txt_ghichu);
        initMap();
    }

    @Override
    public void onNewRequest(String id, String name, String phone, String address, String note, double lat, double lng) {
        this.runOnUiThread(() -> {
            mRequestData = new RequestData(id, name, phone, address, note, lat, lng);
            createDialog(name, phone, address, note, (v) -> {
                mStandbyRadioBtn.toggle();
                mReadyRadioBtn.setEnabled(false);
                mStandbyRadioBtn.setEnabled(false);
                btnBegin.setVisibility(View.VISIBLE);
                btnBegin.setEnabled(true);
                btnEnd.setVisibility(View.VISIBLE);
                btnEnd.setEnabled(false);
                mHereMap.setMarker("user", R.mipmap.picker_user, mRequestData.latitude, mRequestData.longitude, false);
                mHereMap.showRoute("driver", "user");
                mDriverClient.accept(mRequestData);
            }, (v) -> {
                mDriverClient.reject(mRequestData);
                Toast.makeText(MapActivity.this, "Hủy đón khách!", Toast.LENGTH_SHORT).show();
            });
        });
    }

    @Override
    public void onNewToken(String accessToken, String refreshToken) {
        mAccessToken = accessToken;
        mRefreshToken = refreshToken;
    }

    @Override
    public void terminate(String why) {
        this.runOnUiThread(() -> {
            Toast.makeText(MapActivity.this, why, Toast.LENGTH_SHORT).show();
            if (!MapActivity.this.isFinishing()) {
                MapActivity.this.finish();
            }
        });
    }

    private void createDialog(String name, String phone, String address, String note,
                              View.OnClickListener accept,
                              View.OnClickListener reject) {

        Dialog dialog = new Dialog(MapActivity.this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.driver_custom_dialog);

        Button btnDonKhach = dialog.findViewById(R.id.btn_donkhach);
        Button btnTuChoi = dialog.findViewById(R.id.btn_tuchoi);
        TextView txtName = dialog.findViewById(R.id.txt_name);
        TextView txtAddress = dialog.findViewById(R.id.txt_address);
        TextView txtNote = dialog.findViewById(R.id.txt_note);
        TextView txtTimeout = dialog.findViewById(R.id.txt_timeout);

        txtName.setText(name);
        txtAddress.setText(address);
        txtNote.setText(note);

        CountDownTimer timer = createTimer(TIMEOUT, txtTimeout, () -> {
            dialog.cancel();
            reject.onClick(null);
        });

        btnDonKhach.setOnClickListener(v -> {
            timer.cancel();
            dialog.cancel();
            accept.onClick(v);
            setInfoView(name,address,phone,note);
        });
        btnTuChoi.setOnClickListener(v -> {
            timer.cancel();
            dialog.cancel();
            reject.onClick(v);
        });
        dialog.show();
        timer.start();
    }

    private void setInfoView(String name, String address, String phone, String note) {
        info.setVisibility(View.VISIBLE);
        info_name.setText("Tên khách hàng: "+ name);
        info_address.setText("Địa chỉ: "+ address);
        info_phone.setText("Số điện thoại: "+phone);
        info_note.setText("Ghi chú: "+note);
    }

    private void initMap() {
        MapFragment fragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map_fragment);
        mHereMap = new HereMap(fragment);
        mHereMap.setOnTapListener((lat, lng) -> {
            GeoCoordinate g1 = new GeoCoordinate(mDriverClient.getLastKnownLatitude(), mDriverClient.getLastKnownLongitude());
            GeoCoordinate g2 = new GeoCoordinate(lat, lng);
            if (g1.distanceTo(g2) > 100) {
                Toast.makeText(MapActivity.this, "Click quá 100m", Toast.LENGTH_SHORT).show();
                return;
            }
            mDriverClient.setupLocation(lat, lng);
            mDriverClient.updateLocation();
            mHereMap.setMarker("driver", R.mipmap.marker_blue, lat, lng, true);
            Log.d("TAP", String.valueOf(lat) + " " + String.valueOf(lng));
        });
    }

    private void locate() {
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, new GrantResultListener() {
            @Override
            public void onGranted() throws SecurityException {
                if (mLocationReady) {
                    mDriverClient.updateLocation();
                    mHereMap.setMarker("driver", R.mipmap.picker_driver, mDriverClient.getLatitude(), mDriverClient.getLongitude(), true);
                } else {
                    Toast.makeText(MapActivity.this, "Waiting for location service", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onDeny() {
                showPermissionError();
            }
        });
    }

    private boolean checkServicesAvailibility() {
        GoogleApiAvailability apiAbility = GoogleApiAvailability.getInstance();
        int code = apiAbility.isGooglePlayServicesAvailable(this);
        if (code != ConnectionResult.SUCCESS) {
            apiAbility.getErrorDialog(this, code, SERVICE_AVAILABILITY_REQUEST_CODE, dialog -> finish()).show();
            return false;
        }
        return true;
    }

    private void setupLocationServiceSetting() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FAST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult != null) {
                    Location location = locationResult.getLastLocation();
                    double lat = location.getLatitude();
                    double lng = location.getLongitude();
                    mDriverClient.setupLocation(lat, lng);
                    Log.d("LOCATION", String.valueOf(lat) + " " + String.valueOf(lng));
                    mLocationReady = true;
                }
            }
        };

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, locationSettingsResponse -> {
            try {
                mLocationProvider.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        });

        task.addOnFailureListener(this, e -> {
            mLocationReady = false;
            if (e instanceof ResolvableApiException) {
                try {
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    resolvable.startResolutionForResult(MapActivity.this, SERVICE_SETTING_REQUEST_CODE);
                } catch (IntentSender.SendIntentException sendEx) {

                }
            }
        });
    }

    public CountDownTimer createTimer(int Seconds, final TextView tv, Runnable timeout) {
        return new CountDownTimer(Seconds * 1000 + 1000, 1000) {
            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);
                seconds = seconds % 60;
                tv.setText("00:" + String.format("%02d", seconds));
            }

            public void onFinish() {
                timeout.run();
            }
        };
    }
}
