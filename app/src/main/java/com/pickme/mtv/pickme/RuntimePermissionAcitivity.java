package com.pickme.mtv.pickme;

import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class RuntimePermissionAcitivity extends AppCompatActivity {
    private static final int PERMISSION_REQUEST_CODE = 13;

    @Nullable
    private GrantResultListener mGrantResultListener;

    public interface GrantResultListener {
        void onGranted();

        void onDeny();
    }

    protected void requestPermissions(final String[] requestPermissions, GrantResultListener callback) {
        mGrantResultListener = callback;
        final List<String> missingPermissions = new ArrayList<>();
        for (String permission : requestPermissions) {
            if (!(ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED)) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            final String[] permissions = new String[missingPermissions.size()];
            missingPermissions.toArray(permissions);
            ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE);
        } else {
            if (mGrantResultListener != null) {
                mGrantResultListener.onGranted();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            for (int i = permissions.length - 1; i >= 0; i--) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    if (mGrantResultListener != null) {
                        mGrantResultListener.onDeny();
                        return;
                    }
                }
            }
            if (mGrantResultListener != null) {
                mGrantResultListener.onGranted();
            }
        }
    }

    protected void showPermissionError() {
        Toast.makeText(this, "Required permission not granted", Toast.LENGTH_LONG).show();
    }
}
