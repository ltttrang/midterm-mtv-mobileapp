package com.pickme.mtv.pickme;

public class RequestData {
    public String id;
    public String name;
    public String phone;
    public String address;
    public String note;
    public double latitude;
    public double longitude;

    public RequestData(String id, String name, String phone, String address, String note, double lat, double lng) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.note = note;
        this.latitude = lat;
        this.longitude = lng;
    }
}
