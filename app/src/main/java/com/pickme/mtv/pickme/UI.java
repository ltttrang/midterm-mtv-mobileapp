package com.pickme.mtv.pickme;

public interface UI {
    void onNewRequest(String id, String name, String phone, String address, String note, double lat, double lng);
    void onNewToken(String accessToken, String refreshToken);
    void onConnected();
    void terminate(String why);
}
