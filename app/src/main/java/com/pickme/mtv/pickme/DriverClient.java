package com.pickme.mtv.pickme;

import io.socket.client.IO;
import io.socket.client.Socket;

public class DriverClient {
    private DriverData mDriverData;
    private double mLatitude;
    private double mLongitude;

    private double mLastKnownLat;
    private double mLastKnownLng;

    private Socket mSocket;

    private UI mView;

    private boolean first = true;

    public DriverClient(UI view, DriverData data) {
        mView =view;
        this.mDriverData = data;
    }

    public void connect(String url, String accessToken, String refreshToken) throws Exception {
        IO.Options opt = new IO.Options();
        opt.query = "access_token=" + accessToken + "&refresh_token=" + refreshToken + "&role=2";
        mSocket = IO.socket(url, opt);

        mSocket.on(Socket.EVENT_CONNECT, args -> {
            if (mView != null) {
                mView.onConnected();
            }
        });

        mSocket.on("driver/new-request", args -> {
            if (mView != null) {
                String id = (String) args[0];
                String name = (String) args[1];
                String phone = (String) args[2];
                String address = (String) args[3];
                String note = (String) args[4];
                double lat = Double.valueOf((String) args[5]);
                double lng = Double.valueOf((String) args[6]);
                mView.onNewRequest(id, name, phone, address, note, lat, lng);
            }
        });

        mSocket.on(Socket.EVENT_CONNECT_ERROR, args -> {
            if (mView != null) {
                mSocket.disconnect();
                mSocket = null;
                mView.terminate("Cannot connect to server");
            }
        });

        mSocket.connect();
    }

    public void updateStatus(int status) {
        if (mSocket != null && mSocket.connected()) {
            mSocket.emit("driver/update-status", status);
        }
    }

    public void setupLocation(double lat, double lng) {
        mLatitude = lat;
        mLongitude = lng;
        if (first) {
            mLastKnownLat = lat;
            mLastKnownLng = lng;
            first = false;
        }
    }

    public void updateLocation() {
        mLastKnownLat = mLatitude;
        mLastKnownLng =mLongitude;
        if (mSocket != null && mSocket.connected()) {
            mSocket.emit("driver/update-location", mLatitude, mLongitude);
        }
    }

    public void accept(RequestData req) {
        if (mSocket != null && mSocket.connected()) {
            mSocket.emit("driver/accept", req.id);
        }
    }

    public void reject(RequestData req) {
        if (mSocket != null && mSocket.connected()) {
            mSocket.emit("driver/reject", req.id);
        }
    }

    public void begin(RequestData req) {
        if (mSocket != null && mSocket.connected()) {
            mSocket.emit("driver/begin", req.id);
        }
    }

    public void end(RequestData req) {
        if (mSocket != null && mSocket.connected()) {
            mSocket.emit("driver/end", req.id);
        }
    }

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public double getLastKnownLatitude() {
        return mLastKnownLat;
    }

    public double getLastKnownLongitude() {
        return mLastKnownLng;
    }

    public void disconnect() {
        if (mSocket != null && mSocket.connected()) {
            mSocket.disconnect();
        }
    }

    public boolean isConntected() {
        if (mSocket != null) {
            return mSocket.connected();
        }
        return false;
    }
}
