package com.pickme.mtv.pickme.heremap;

import android.graphics.PointF;

import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapFragment;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.RouteManager;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class HereMap implements RouteManager.Listener {
    public static final int DEFAULT_ZOOM_LEVEL = 15;
    private Map mMap;
    private RouteManager mRouteManager;
    private HashMap<String, MapMarker> mMarkerMap;
    private MapRoute mRoute;
    private OnTapListener mTapListener;

    private static final GeoCoordinate CENTER_POINT = new GeoCoordinate(10.799513333333334, 106.71217166666665, 0.0);

    public interface OnTapListener {
        void onTap(double lat, double lng);
    }

    public HereMap(MapFragment mapFragment) {
        mMarkerMap = new HashMap<>();
        mRouteManager = new RouteManager();
        mapFragment.init(error -> {
            if (error == OnEngineInitListener.Error.NONE) {
                mMap = mapFragment.getMap();
                mMap.setCenter(CENTER_POINT, Map.Animation.NONE);
                mMap.setZoomLevel((mMap.getMaxZoomLevel() + mMap.getMinZoomLevel()) / 2);
                mapFragment.getMapGesture().addOnGestureListener(new BaseGestureListener() {
                    @Override
                    public boolean onTapEvent(PointF pointF) {
                        if (mTapListener != null) {
                            GeoCoordinate location = mMap.pixelToGeo(pointF);
                            mTapListener.onTap(location.getLatitude(), location.getLongitude());
                        }
                        return true;
                    }
                });
            }
        });
    }

    public void setMarker(String name, int icon, double lat, double lng, boolean center) {
        if (mMarkerMap.containsKey(name)) {
            mMap.removeMapObject(mMarkerMap.get(name));
            mMarkerMap.remove(name);
        }
        GeoCoordinate location = new GeoCoordinate(lat, lng);
        Image img = getImage(icon);
        if (img != null) {
            MapMarker marker = new MapMarker(location, img);
            mMarkerMap.put(name, marker);
            mMap.addMapObject(marker);
            if (center) {
                mMap.setCenter(location, Map.Animation.NONE);
            }
            if (mMap.getZoomLevel() < DEFAULT_ZOOM_LEVEL) {
                mMap.setZoomLevel(DEFAULT_ZOOM_LEVEL);
            }
        }
    }

    public void showRoute(String from, String to) {
        if (!mMarkerMap.containsKey(from) || !mMarkerMap.containsKey(to)) {
            return;
        }
        MapMarker markerA = mMarkerMap.get(from);
        MapMarker markerB = mMarkerMap.get(to);
        if (markerA != null && markerB != null) {
            RoutePlan routePlan = new RoutePlan();
            routePlan.addWaypoint(new GeoCoordinate(markerA.getCoordinate().getLatitude(),
                    markerA.getCoordinate().getLongitude()));
            routePlan.addWaypoint(new GeoCoordinate(markerB.getCoordinate().getLatitude(),
                    markerB.getCoordinate().getLongitude()));
            RouteOptions routeOptions = new RouteOptions();
            routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
            routeOptions.setRouteType(RouteOptions.Type.FASTEST);
            routePlan.setRouteOptions(routeOptions);
            mRouteManager.calculateRoute(routePlan, this);
        }
    }

    @Override
    public void onProgress(int i) {
        // dismiss
    }

    @Override
    public void onCalculateRouteFinished(RouteManager.Error error, List<RouteResult> list) {
        if (error == RouteManager.Error.NONE) {
            mRoute = new MapRoute(list.get(0).getRoute());
            mMap.addMapObject(mRoute);
        }
    }

    public void removeRoute() {
        if (mRoute != null) {
            mMap.removeMapObject(mRoute);
            mRoute = null;
        }
    }

    public void removeMarker(String name) {
        if (mMarkerMap.containsKey(name)) {
            mMap.removeMapObject(mMarkerMap.get(name));
        }
    }

    public void setOnTapListener(OnTapListener listener) {
        mTapListener = listener;
    }

    private Image getImage(int resource) {
        Image img = new Image();
        try {
            img.setImageResource(resource);
        } catch (IOException e) {
            return null;
        }
        return img;
    }
}
