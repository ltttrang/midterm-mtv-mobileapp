package com.pickme.mtv.pickme;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pickme.mtv.pickme.lib.HttpRequest;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    Button btn_dangnhap;
    EditText mUsernameEdt;
    EditText mPasswordEdt;

    private ProgressDialog mProgressDialog;

    public class Response {
        public String access_token;
        public String refresh_token;
        public String username;
        public String name;
        public String phone;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        init();
    }

    private void init() {
        btn_dangnhap = findViewById(R.id.btn_dangnhap);
        mUsernameEdt = findViewById(R.id.username_edt);
        mPasswordEdt = findViewById(R.id.pass_edt);
        btn_dangnhap.setOnClickListener(v -> {
            action();
        });
    }

    private void action() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText("http://192.168.1.3:8080");
        builder.setTitle("Nhập url của server");
        builder.setView(input);
        builder.setPositiveButton("Đồng ý", (dialog, which) -> {
            dialog.cancel();
            String url = input.getText().toString();
            login(url, mUsernameEdt.getText().toString(), mPasswordEdt.getText().toString());
        });
        builder.setNegativeButton("Hủy", (dialog, which) -> dialog.cancel());
        builder.show();
    }

    private void login(String url, String username, String pass) {
        mProgressDialog = ProgressDialog.show(this, "Loading", "Connecting...");
        (new Thread(() -> {
            try {
                Map<String, String> body = new HashMap<>();
                body.put("username", username);
                body.put("password", pass);
                HttpRequest req = HttpRequest.post(url + "/logon").connectTimeout(5000).form(body);
                int code = req.code();
                if (code == 200) {
                    String jsonStr = req.body();
                    Gson gson = new Gson();
                    Response res = gson.fromJson(jsonStr, Response.class);
                    next(url, new DriverData(res.username, res.name, res.phone), res.access_token, res.refresh_token);
                } else {
                    showError(req.body());
                }
            } catch (Exception e) {
                showError("Không thể kết nối tới server");
            } finally {
                runOnUiThread(() -> {
                    if (mProgressDialog != null && mProgressDialog.isShowing()) {
                        mProgressDialog.cancel();
                    }
                });
            }
        })).start();
    }

    private void next(String url, DriverData data, String atk, String rtk) {
        runOnUiThread(() -> {
            Intent intent = new Intent(LoginActivity.this, MapActivity.class);
            intent.putExtra("url", url);
            intent.putExtra("driver_data,", data);
            intent.putExtra("access_token", atk);
            intent.putExtra("refresh_token", rtk);
            startActivity(intent);
        });
    }

    private void showError(String what) {
        runOnUiThread(() -> {
            Toast.makeText(LoginActivity.this, what, Toast.LENGTH_SHORT).show();
        });
    }

}
