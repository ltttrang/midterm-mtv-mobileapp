package com.pickme.mtv.pickme;

import android.os.Parcel;
import android.os.Parcelable;

public class DriverData implements Parcelable {
    public String username;
    public String name;
    public String phone;

    public DriverData(String username, String name, String phone) {
        this.username= username;
        this.name= name;
        this.phone = phone;
    }

    protected DriverData(Parcel in) {
        username = in.readString();
        name = in.readString();
        phone = in.readString();
    }

    public static final Creator<DriverData> CREATOR = new Creator<DriverData>() {
        @Override
        public DriverData createFromParcel(Parcel in) {
            return new DriverData(in);
        }

        @Override
        public DriverData[] newArray(int size) {
            return new DriverData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(name);
        dest.writeString(phone);
    }
}
